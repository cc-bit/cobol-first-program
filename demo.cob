01 record-count pic s9(8) comp-5.
01 table.
   05 table-entry occurs "thousands" depending upon record-count.
   05 table-record       pic x(100)
   05 filler redefines table-record.
      10 table-Acct-Num  pic x(7).
      10 table-Trans     pic x(3).
      10 table-Amt       pic x(3).
   05 table-Flag         pic x.

move zero to record-count.
move low-values to table.
open file-1
perform until file-1-eof
   read file-1
      at-end set
         file-1-eof to true
      not-at-end
         add 1 to record-count
         move record-1 to table-entry (record-count)
         move 'n' to table-flag (record-count)
   end-read
end-perform
close file-1
open file-2
perform until file-2-eof
   read file-2
      at-end set
         file-2-eof to true
      not-at-end
         perform varying x from 1 by 1
                 until x > record-count
            if record-2-Acct-Num = table-Acct-Num and
               record-2-Trans    = table-Trans    and
               record-2-Amt      = table-Amt
                  move 'y' to table-flag (x)
                  move record-count to x
            end-if
         end-perform
      end-read
end-perform
close file-2
goback
            >>SOURCE FORMAT FREE
            IDENTIFICATION DIVISION.
            PROGRAM-ID. P2.
            AUTHOR. Chris Chitwood
            DATE-WRITTEN. March 4, 2021
            ENVIRONMENT DIVISION.
            INPUT-OUTPUT SECTION.

            FILE-CONTROL.
                SELECT CUSTOMER_FILE ASSIGN TO "customers.dat"
                    ORGANIZATION IS LINE SEQUENTIAL.
                SELECT INVENTORY_FILE ASSIGN TO "inventory.dat"
                    ORGANIZATION IS LINE SEQUENTIAL.
                SELECT TX_FILE ASSIGN TO "transactions.dat"
                    ORGANIZATION IS LINE SEQUENTIAL.
                SELECT ERROR_FILE ASSIGN TO "errors.txt"
                    ORGANIZATION IS LINE SEQUENTIAL.
                SELECT PROCESSED_FILE ASSIGN TO "processed.dat"
                    ORGANIZATION IS LINE SEQUENTIAL.
                SELECT ORDERS_FILE ASSIGN TO "orders.txt"
                    ORGANIZATION IS LINE SEQUENTIAL.

            DATA DIVISION.
            FILE SECTION.
            *> Tabel definitions for import and export.
            FD CUSTOMER_FILE.
            01 CUSTOMER_TABLE.
                03 CUSTOMER OCCURS 10 TIMES.
                    05 CUSTOMERID       PIC 99999.
                    05 CUSTOMERNAME     PIC X(22).
                    05 CUSTOMERADDRESS  PIC X(25).
                    05 CUSTOMERCITY     PIC X(17).
                    05 CUSTOMERGEO      PIC X(12).
                    05 CUSTOMERBALANCE  PIC 999.99.
            
            FD INVENTORY_FILE.
            01 INVENTORYTABLE.
                03 PRODUCT OCCURS 25 TIMES.
                    05 PRODUCTID       PIC x(6).
                    05 PRODUCTNAME     PIC X(29).
                    05 PRODUCTSTOCK    PIC X(7).
                    05 PRODUCTREORDER  PIC X(7).
                    05 PRODUCTPRICE    PIC X(12).

            FD TX_FILE.
            01 TXTABLE.
                03 TX OCCURS 21 TIMES.
                    05 TXCUSTOMERID     PIC 99999.
                    05 TXPRODUCTID      PIC X(15).
                    05 TXORDERAMOUNT    PIC X(3).
                    05 TXCODE           PIC X(6).
            FD ERROR_FILE.
            01 ERRORTABLE.
                03 ERROR_ENTRY OCCURS 25 TIMES.
                    05 ERROR_CUSTOMERID  PIC X(6).
                    05 ERROR_PRODUCTID   PIC X(12).
                    05 ERROR_ORDERAMOUNT PIC X(4).
                    05 ERROR_TYPE        PIC X(25).

            FD PROCESSED_FILE.
            01 PROCESSEDTABLE.
                03 PROCESSED OCCURS 19 TIMES.
                    05 PROCESSED_CUSTOMER_NAME  PIC X(22).
                    05 PROCESSED_ADDRESS        PIC X(22).
                    05 PROCESSED_ITEM_NAME      PIC X(29).
                    05 PROCESSED_AMOUNT         PIC 9.
                    05 PROCESSED_SPACER1        PIC X(3).
                    05 PROCESSED_GROSS          PIC 999.99.
                    05 PROCESSED_SPACER2        PIC X(3).
                    05 PROCESSED_DISCOUNT       PIC X(10).
                    05 PROCESSED_SPACER3        PIC X(3).
                    05 PROCESSED_NET            PIC 999.99.
                    05 PROCESSED_SPACER4        PIC X(3).
                    05 PROCESSED_BAL            PIC 999.99.

            FD ORDERS_FILE.
            01 ORDERS_TABLE.
                03 ORDERS OCCURS 21 TIMES.
                    05 ORDERS_ITEMNAME PIC X(25).
                    05 ORDERS_QUANTITY PIC 99.
            WORKING-STORAGE SECTION.

            *> Flags and counters
            01 VALID_CUSTOMER PIC 1 VALUE 1.
            01 VALID_PRODUCT PIC 1 VALUE 1.
            01 CUST_STEP PIC 99 VALUE 1.
            01 VALID_CUSTOMER_INDEX PIC 99 VALUE 1.
            01 VALID_PRODUCT_INDEX PIC 99 VALUE 1.
            01 PROD_STEP PIC 99 VALUE 1.            
            01 TX_STEP PIC 99 VALUE 1.
            01 ERROR_COUNT PIC 99 VALUE 0.
            01 INDEX_COUNT PIC 99 VALUE 0.
            01 WS_EOF PIC A(1).
            01 PROCESSED_COUNT PIC 99 VALUE 0.
            01 ORDER_COUNT PIC 99 VALUE 0.
            
            *> Variables for holding prices.
            01 GROSS_COST PIC 999V99.
            01 NET_COST PIC 999V99.
            
            *> For an easier type casting.
            01 NUMERIC_VALUE PIC 999999.

            *> variables to help me get around directly dealing with floats.
            01 CURRENT_FLOAT_PRICE PIC 99V99.
            01 CURRENT_ORDERAMOUNT PIC 99.
            01 CURRENT_BALANCE PIC 999V99.
            01 NEW_BALANCE PIC 999V99.
            01 ORDERMIN PIC 99.
            01 CURRENT_STOCK PIC 99.
            01 ORDER_QUANTITY PIC 99.
            01 CHECK_ZERO pic 99999.

            *> Discount code and description.
            01 DISCOUNT_CODE PIC X(1).
            01 DISCOUNT_DESC PIC X(23).

            *> Working Section tables to not modify original files.
            01 WS_CUSTOMERTABLE.
                03 WS_CUSTOMER OCCURS 10 TIMES.
                    05 WS_CUSTOMERID       PIC 99999.
                    05 WS_CUSTOMERNAME     PIC X(22).
                    05 WS_CUSTOMERADDRESS  PIC X(25).
                    05 WS_CUSTOMERCITY     PIC X(17).
                    05 WS_CUSTOMERGEO      PIC X(12).
                    05 WS_CUSTOMERBALANCE  PIC 999v99.

            01 WS_INVENTORYTABLE.
                03 WS_PRODUCT OCCURS 25 TIMES.
                    05 WS_PRODUCTID       PIC x(6).
                    05 WS_PRODUCTNAME     PIC X(29).
                    05 WS_PRODUCTSTOCK    PIC X(7).
                    05 WS_PRODUCTREORDER  PIC X(7).
                    05 WS_PRODUCTPRICE    PIC X(12).
            
            01 WS_TXTABLE.
                03 WS_TX OCCURS 21 TIMES.
                    05 WS_TXCUSTOMERID     PIC 99999.
                    05 WS_TXPRODUCTID      PIC X(15).
                    05 WS_TXORDERAMOUNT    PIC X(3).
                    05 WS_TXCODE           PIC X(6).

            01 WS_ERRORTABLE.
                03 WS_ERROR_ENTRY OCCURS 25 TIMES.
                    05 WS_ERROR_CUSTOMERID  PIC X(6).
                    05 WS_ERROR_PRODUCTID   PIC X(12).
                    05 WS_ERROR_ORDERAMOUNT PIC X(4).
                    05 WS_ERROR_TYPE        PIC X(25).
        
            01 WS_PROCESSEDTABLE.
                03 WS_PROCESSED OCCURS 19 TIMES.
                    05 WS_PROCESSED_CUSTOMER_NAME  PIC X(22).
                    05 WS_PROCESSED_ADDRESS        PIC X(22).
                    05 WS_PROCESSED_ITEM_NAME      PIC X(29).
                    05 WS_PROCESSED_AMOUNT         PIC 9.
                    05 WS_PROCESSED_SPACER1        PIC X(3).
                    05 WS_PROCESSED_GROSS          PIC 999.99.
                    05 WS_PROCESSED_SPACER2        PIC X(3).
                    05 WS_PROCESSED_DISCOUNT       PIC X(23).
                    05 WS_PROCESSED_SPACER3        PIC X(3).
                    05 WS_PROCESSED_NET            PIC 999.99.
                    05 WS_PROCESSED_SPACER4        PIC X(3).
                    05 WS_PROCESSED_BAL            PIC 999.99.

            01 WS_ORDERS_TABLE.
                03 WS_ORDERS OCCURS 21 TIMES.
                    05 WS_ORDERS_ITEMNAME PIC X(25).
                    05 WS_ORDERS_QUANTITY PIC 99.
            
            PROCEDURE DIVISION.

            *> Read file into table.
            OPEN INPUT CUSTOMER_FILE.                
            PERFORM UNTIL WS_EOF = 'Y'
                READ CUSTOMER_FILE
                    AT END
                        MOVE 'Y' TO WS_EOF
                    NOT AT END
                        ADD 1 TO INDEX_COUNT
                        MOVE CUSTOMER(1) TO WS_CUSTOMER(INDEX_COUNT)
                END-READ
            END-PERFORM

            CLOSE CUSTOMER_FILE.
            
            *> Reset counter and eof flag.
            MOVE 'N' TO WS_EOF.
            MOVE '0' TO INDEX_COUNT. 
            
            OPEN INPUT INVENTORY_FILE.
                
            PERFORM UNTIL WS_EOF = 'Y' OR INDEX_COUNT = 25
                READ INVENTORY_FILE
                    AT END
                        MOVE 'Y' TO WS_EOF
                    NOT AT END
                        ADD 1 TO INDEX_COUNT
                        MOVE PRODUCT(1) TO WS_PRODUCT(INDEX_COUNT)
                END-READ
            END-PERFORM
            CLOSE INVENTORY_FILE.
            
            *> Reset counter and eof flag.
            MOVE 'N' TO WS_EOF.
            MOVE '0' TO INDEX_COUNT
            
            *> Read file into table
            OPEN INPUT TX_FILE. 
            PERFORM UNTIL WS_EOF = 'Y' OR INDEX_COUNT = 25
                READ TX_FILE
                    AT END
                        MOVE 'Y' TO WS_EOF
                    NOT AT END
                        ADD 1 TO INDEX_COUNT
                        MOVE TX(1) TO WS_TX(INDEX_COUNT) 
                END-READ
            END-PERFORM

            
            CLOSE TX_FILE.

            *> To run through all transactions.
            PERFORM UNTIL TX_STEP = 22
                *> This will run through all customers and check if the id is valid.
                PERFORM UNTIL CUST_STEP = 25 OR VALID_CUSTOMER = 1
                    IF WS_TXCUSTOMERID(TX_STEP) IS EQUAL TO WS_CUSTOMERID(CUST_STEP) THEN
                        MOVE 1 TO VALID_CUSTOMER
                        *> Grab index for processing.
                        MOVE CUST_STEP TO VALID_CUSTOMER_INDEX
                    END-IF

                    *> End of current customer.  Move to next customer.
                    ADD 1 TO CUST_STEP
                END-PERFORM

                *> Runs through the products.
                PERFORM UNTIL PROD_STEP = 25 OR VALID_PRODUCT = 1
                    *> Using this for matching types
                    MOVE WS_TXPRODUCTID(TX_STEP) TO NUMERIC_VALUE
                    IF NUMERIC_VALUE IS EQUAL TO WS_PRODUCTID(PROD_STEP) THEN
                        MOVE 1 TO VALID_PRODUCT

                        *> Grab index for processing.
                        MOVE PROD_STEP TO VALID_PRODUCT_INDEX
                    END-IF


                    *> End of current product.  Move to next product.
                    ADD 1 TO PROD_STEP
                END-PERFORM

                IF VALID_CUSTOMER = 1 AND VALID_PRODUCT = 1 THEN
                    ADD 1 TO PROCESSED_COUNT
                    MOVE WS_PRODUCTSTOCK(VALID_PRODUCT_INDEX) TO CURRENT_STOCK
                    MOVE WS_PRODUCTPRICE(VALID_PRODUCT_INDEX) TO CURRENT_FLOAT_PRICE
                    MOVE WS_TXORDERAMOUNT(TX_STEP) TO CURRENT_ORDERAMOUNT
                    MULTIPLY CURRENT_FLOAT_PRICE BY CURRENT_ORDERAMOUNT GIVING GROSS_COST
                   
                    

                    *> Given my implementation, the 6th character in the string is the discount code.
                    MOVE WS_TXCODE(TX_STEP)(6:1) TO DISCOUNT_CODE

                    EVALUATE TRUE
                        WHEN DISCOUNT_CODE IS EQUAL TO 'A'
                            MULTIPLY GROSS_COST BY 0.9 GIVING NET_COST
                            MOVE "10% off" TO DISCOUNT_DESC
                        WHEN DISCOUNT_CODE IS EQUAL TO 'B'
                            MULTIPLY GROSS_COST BY 0.8 GIVING NET_COST
                            MOVE "20% off" TO DISCOUNT_DESC
                        WHEN DISCOUNT_CODE IS EQUAL TO 'C'
                            MULTIPLY GROSS_COST BY 0.75 GIVING NET_COST
                            MOVE "25% off" TO DISCOUNT_DESC
                        WHEN DISCOUNT_CODE IS EQUAL TO 'D'
                            IF CURRENT_ORDERAMOUNT >= 3 THEN
                                SUBTRACT CURRENT_FLOAT_PRICE FROM GROSS_COST GIVING NET_COST
                            END-IF 
                            MOVE "1 free with at least 3." TO DISCOUNT_DESC
                        WHEN DISCOUNT_CODE IS EQUAL TO 'E'
                            MULTIPLY GROSS_COST BY 0.5 GIVING NET_COST
                            MOVE "Buy 1, get 1 free." TO DISCOUNT_DESC
                        WHEN DISCOUNT_CODE IS EQUAL TO 'Z'
                            MOVE GROSS_COST TO NET_COST
                            MOVE "No Discount." TO DISCOUNT_DESC
                        WHEN OTHER
                            DISPLAY "UNKNOWN DISCOUNT CODE"
                    END-EVALUATE
                    
                    *> Grab the customer's current balance.
                    MOVE WS_CUSTOMERBALANCE(VALID_CUSTOMER_INDEX) TO CURRENT_BALANCE
                    
                    *> Floats in COBOL are driving me crazy.
                    MOVE CURRENT_BALANCE TO CHECK_ZERO
                    IF CHECK_ZERO = 0 THEN 
                        MOVE NET_COST TO NEW_BALANCE
                    ELSE
                        ADD NET_COST TO CURRENT_BALANCE GIVING NEW_BALANCE
                    END-IF

                    *> Export data to the processed table to be written to a file
                    MOVE WS_PRODUCTNAME(VALID_PRODUCT_INDEX) TO WS_PROCESSED_ITEM_NAME(PROCESSED_COUNT)
                    MOVE WS_CUSTOMERNAME(VALID_CUSTOMER_INDEX) TO WS_PROCESSED_CUSTOMER_NAME(PROCESSED_COUNT)
                    MOVE WS_CUSTOMERADDRESS(VALID_CUSTOMER_INDEX) TO WS_PROCESSED_ADDRESS(PROCESSED_COUNT)
                    MOVE CURRENT_ORDERAMOUNT TO WS_PROCESSED_AMOUNT(PROCESSED_COUNT)
                    MOVE GROSS_COST TO WS_PROCESSED_GROSS(PROCESSED_COUNT)
                    MOVE DISCOUNT_DESC TO WS_PROCESSED_DISCOUNT(PROCESSED_COUNT)
                    MOVE NET_COST TO WS_PROCESSED_NET(PROCESSED_COUNT)
                    MOVE NEW_BALANCE TO WS_PROCESSED_BAL(PROCESSED_COUNT)

                    *> Grab the mininum point reorder and decrease stock buy the amount bought.
                    MOVE WS_PRODUCTREORDER(VALID_PRODUCT_INDEX) TO ORDERMIN
                    SUBTRACT CURRENT_ORDERAMOUNT FROM CURRENT_STOCK
                    
                    *> Logic for handling reorders.
                    IF CURRENT_STOCK <= ORDERMIN
                        ADD 1 TO ORDER_COUNT

                        EVALUATE TRUE
                            WHEN ORDERMIN = 1
                                MOVE 3 TO ORDER_QUANTITY
                            WHEN ORDERMIN >= 2 AND ORDERMIN <= 5 
                                MOVE 6 TO ORDER_QUANTITY
                            WHEN ORDERMIN >= 6 AND ORDERMIN <= 10
                                MOVE 12 TO ORDER_QUANTITY
                            WHEN ORDERMIN >= 11 AND <= 20
                                MOVE 25 TO ORDER_QUANTITY
                            WHEN OTHER
                                MOVE 30 TO ORDER_QUANTITY
                        END-EVALUATE
                        MOVE WS_PRODUCTNAME(VALID_PRODUCT_INDEX) TO WS_ORDERS_ITEMNAME(ORDER_COUNT)
                        MOVE ORDER_QUANTITY TO WS_ORDERS_QUANTITY(ORDER_COUNT)
                        
                    END-IF
                    


                ELSE
                    *> Count the errors and record the data.
                    ADD 1 TO ERROR_COUNT
                    IF VALID_CUSTOMER = 0 THEN
                        MOVE "invalid product number" TO WS_ERROR_TYPE(ERROR_COUNT)
                    END-IF
                    IF VALID_PRODUCT = 0 THEN
                        MOVE "invalid customer number" TO WS_ERROR_TYPE(ERROR_COUNT)
                    END-IF
                    MOVE WS_TXCUSTOMERID(TX_STEP) TO WS_ERROR_CUSTOMERID(ERROR_COUNT)
                    MOVE WS_TXPRODUCTID(TX_STEP) TO WS_ERROR_PRODUCTID(ERROR_COUNT)
                    MOVE WS_TXORDERAMOUNT(TX_STEP) TO WS_ERROR_ORDERAMOUNT(ERROR_COUNT)

                END-IF
                
            *> End of current tx.  Reset customer and product counters
            *> Reset valid customer and product flags.
            ADD 1 TO TX_STEP
            MOVE 1 TO CUST_STEP
            MOVE 1 TO PROD_STEP
            MOVE 0 TO VALID_CUSTOMER
            MOVE 0 TO VALID_PRODUCT

            END-PERFORM

            *> Export tables to files.
            OPEN OUTPUT ERROR_FILE.
                MOVE WS_ERRORTABLE TO ERRORTABLE
                WRITE ERRORTABLE
                END-WRITE
            CLOSE ERROR_FILE
            
            OPEN OUTPUT PROCESSED_FILE.
                MOVE WS_PROCESSEDTABLE TO PROCESSEDTABLE
                WRITE PROCESSEDTABLE
                END-WRITE
            CLOSE PROCESSED_FILE

            OPEN OUTPUT ORDERS_FILE.
                MOVE WS_ORDERS_TABLE TO ORDERS_TABLE
                WRITE ORDERS_TABLE
                END-WRITE
            CLOSE ORDERS_FILE

            

            STOP RUN.
